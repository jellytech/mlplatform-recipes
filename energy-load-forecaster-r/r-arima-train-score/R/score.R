# Detect proper script_path (you cannot use args yet as they are build with tools in set_env.r)
script_path <- (function() {
  args <- commandArgs(trailingOnly = FALSE)
  script_path <- dirname(sub("--file=", "", args[grep("--file=", args)]))
  if (!length(script_path)) {
    return("R")
  }
  if (grepl("darwin", R.version$os)) {
    base <- gsub("~\\+~", " ", base) # on MacOS ~+~ in path denotes whitespace
  }
  return(normalizePath(script_path))
})()

# Setting .libPaths() to point to libs folder
source(file.path(script_path, "set_env.R"), chdir = T)

config <- load_config()
args <- args_parser()

# arg: ModelInstLoadDir
model_inst_load_dir <- args$get(name = "ModelInstLoadDir", required = FALSE, default = config$ModelInstLoadDir)
model_inst_load_dir <- gsub("<prj_root>", file.path(script_path, ".."), model_inst_load_dir, fixed = TRUE)
assert(length(model_inst_load_dir) == 1 && dir.exists(model_inst_load_dir),
       "Model instance load folder(%s) does not exist", paste(model_inst_load_dir, collapse = ","))

# arg: ModelInstId
model_inst_id <- args$get(name = "ModelInstId", required = FALSE, default = NA)
if (is.na(model_inst_id)) {
  inst_files <- list.files(pattern = "model.Rds", path = model_inst_load_dir, recursive = TRUE, full.names = TRUE)
  assert(length(inst_files) > 0, "No trained model instances found in %s.", model_inst_load_dir)

  finfos <- file.info(inst_files)
  model_inst_fpath <- rownames(finfos[order(finfos$mtime, decreasing = TRUE), ][1,])
  model_inst_id <- basename(dirname(model_inst_fpath))
} else {
  model_inst_fpath <- file.path(model_inst_load_dir, model_inst_id, "model.Rds")
  assert(file.exists(model_inst_fpath),
         "Model(%s) not found in %s. (expected model.Rds does not exist)",
         model_id, model_load_dir)
}
model_inst_fpath <- normalizePath(model_inst_fpath)

# arg: ScoringSaveDir
scoring_save_dir <- args$get(name = "ScoringSaveDir", required = FALSE, default = config$ScoringSaveDir)
assert(!is.null(scoring_save_dir), "ScoringSaveDir argument is required")

scoring_save_dir <- gsub("<prj_root>", file.path(script_path, ".."), scoring_save_dir, fixed = TRUE)
if (!dir.exists(scoring_save_dir)) {
  success <- dir.create(scoring_save_dir, showWarnings = FALSE, recursive = TRUE)
  assert(success, "Failed to create folder: %s", scoring_save_dir)
}

# ScoringId
scoring_id <- args$get(name = "ScoringId", required = FALSE, default = format(Sys.time(), "%Y%m%d_%H%M_scoring"))


suppressPackageStartupMessages({
  library(WhiskyCore)
  library(FeatureStore)
  library(ScoringRepo)
  library(dplyr)
  library(forecast)
})

loginfo("Loading model ...")

model <- readRDS(model_inst_fpath)

# retrieve 'ylog_applied' from model info
info_fpath <- file.path(dirname(model_inst_fpath), "infos.json")
ylog_applied <- tryCatch({
  all(as.logical(jsonlite::read_json(info_fpath)$ylog_applied))
}, error = function(e) {
  logwarn("Failed to read model information from %s; using defaults", info_fpath)
  return(FALSE)
})

loginfo("... done; Connecting to FS ...")

# retrieve data set definition
dataset_def <- WhiskyCore::get_dataset_def(script_path)
conn <- FeatureStore::fs_connect(fs_id = dataset_def$fs_id)

loginfo("... done; Retrieving features ...")

tryCatch({
  fcast_start_date <- FeatureStore::fs_get_obj_data_range(conn, dataset_def$object)$till
  assert(!is.na(fcast_start_date),
         "no modeling object data detected; could not detect forecast start date")
  fcast_start_date <- fcast_start_date + 1

  fcast_end_date <- FeatureStore::fs_get_obj_data_range(conn, "temp_fcast")$till
  assert(!is.na(fcast_end_date) && fcast_start_date <= fcast_end_date,
         "not enough temperature forecast data to perform load forecast")

  temps_fcast_tbl <- FeatureStore::fs_get_feats_tbl(
    conn,
    object = dataset_def$object,   # "load",
    features = dataset_def$features, # TODO: "yearly_mean_degc{year+1L}"
    data_range = list(from = fcast_start_date, till = fcast_end_date))

  loginfo("... done; Performing imputations ...")

  # default value for yearly_mean_degc
  total_yearly_mean_degc <-
    FeatureStore::fs_raw_get_tbl(conn, "temps_yearly") %>%
    summarise(mean(yearly_mean_degc, na.rm = TRUE)) %>%
    pull()

  temps_fcast_tbl <- temps_fcast_tbl %>%
    mutate(avg_temp_degc = ifelse(is.na(avg_temp_degc), avg_temp_fc_degc, avg_temp_degc), # impute temp with forecasts
           yearly_mean_degc = ifelse(is.na(yearly_mean_degc), !!total_yearly_mean_degc, yearly_mean_degc)) %>% # impute yearly mean
    select(-avg_temp_fc_degc)

  loginfo("... done; Transforming features ...")

  temps_fcast_tbl <- temps_fcast_tbl %>%
    mutate(season_adj_temp = ifelse(winter == 1, 2 * yearly_mean_degc - avg_temp_degc, avg_temp_degc))

  loginfo("... done; Generating forecast ...")

  loads_fcast <- forecast(model,
                          h = temps_fcast_tbl %>% summarise(n = n()) %>% pull(),
                          xreg = temps_fcast_tbl %>%
                            select(weekend, season_adj_temp) %>%
                            as.data.frame() %>%
                            as.matrix())

  if (ylog_applied) {
    back_trans <- function(x) exp(as.numeric(x)) - 1
  } else {
    back_trans <- function(x) as.numeric(x)
  }
  loads_fcast_tbl <- tibble(date = temps_fcast_tbl %>% select(date) %>% pull(),
                            avg_load = back_trans(loads_fcast$mean),
                            conf_bound_lower_80 = back_trans(loads_fcast$lower[, 1]),
                            conf_bound_upper_80 = back_trans(loads_fcast$upper[, 1]),
                            conf_bound_lower_95 = back_trans(loads_fcast$lower[, 2]),
                            conf_bound_upper_95 = back_trans(loads_fcast$upper[, 2]))

  loginfo("... done; Saving forecast ...")

  score_dpath <- ScoringRepo::sr_save_scoring(
    model_inst_id, scoring_id, scoring_save_dir, loads_fcast_tbl,
    tag = ""
  )

  loginfo("All done. (saved into %s)", score_dpath)
},
error = function(e) {
  logerror("Failed: %s", conditionMessage(e))
  stop(e)
},
finally = {
  close(conn)
})

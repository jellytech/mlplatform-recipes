# Detect proper script_path (you cannot use args yet as they are build with tools in set_env.r)
script_path <- (function() {
  args <- commandArgs(trailingOnly = FALSE)
  script_path <- dirname(sub("--file=", "", args[grep("--file=", args)]))
  if (!length(script_path)) {
    return("R")
  }
  if (grepl("darwin", R.version$os)) {
    base <- gsub("~\\+~", " ", base) # on MacOS ~+~ in path denotes whitespace
  }
  return(normalizePath(script_path))
})()

# Setting .libPaths() to point to libs folder
source(file.path(script_path, "set_env.R"), chdir = T)

config <- load_config()
args <- args_parser()

# arg: ModelInstLoadDir
model_inst_load_dir <- args$get(name = "ModelInstLoadDir", required = FALSE, default = config$ModelInstLoadDir)
model_inst_load_dir <- gsub("<prj_root>", file.path(script_path, ".."), model_inst_load_dir, fixed = TRUE)
assert(length(model_inst_load_dir) == 1 && dir.exists(model_inst_load_dir),
       "Model instance load folder(%s) does not exist", paste(model_inst_load_dir, collapse = ","))

# arg: ModelInstId
model_inst_id <- args$get(name = "ModelInstId", required = FALSE, default = NA)
if (is.na(model_inst_id)) {
  trans_files <- list.files(pattern = "transform.RDS", path = model_inst_load_dir, recursive = TRUE, full.names = TRUE)
  assert(length(trans_files) > 0, "No trained model instances found in %s.", model_inst_load_dir)

  finfos <- file.info(trans_files)
  trans_fpath <- rownames(finfos[order(finfos$mtime, decreasing = TRUE), ][1,])
  model_inst_dir <- dirname(trans_fpath)
  model_inst_id <- basename(model_inst_dir)
} else {
  model_inst_dir <- file.path(model_inst_load_dir, model_inst_id)
  assert(dir.exists(model_inst_dir), "Model(%s) not found in %s.", model_id, model_load_dir)
}
model_inst_dir <- normalizePath(model_inst_dir)

# arg: ScoringSaveDir
scoring_save_dir <- args$get(name = "ScoringSaveDir", required = FALSE, default = config$ScoringSaveDir)
assert(!is.null(scoring_save_dir), "ScoringSaveDir argument is required")

scoring_save_dir <- gsub("<prj_root>", file.path(script_path, ".."), scoring_save_dir, fixed = TRUE)
if (!dir.exists(scoring_save_dir)) {
  success <- dir.create(scoring_save_dir, showWarnings = FALSE, recursive = TRUE)
  assert(success, "Failed to create folder: %s", scoring_save_dir)
}

# ScoringId
scoring_id <- args$get(name = "ScoringId", required = FALSE, default = format(Sys.time(), "%Y%m%d_%H%M_scoring"))


suppressPackageStartupMessages({
  library(FeatureStore)
  library(ScoringRepo)
  library(WhiskyCore)
  library(H2OHelpers)
  library(dplyr)
  library(h2o)
})

loginfo("Loading model ...")
tryCatch({
  preprocess_and_load_abt <- readRDS(file = file.path(model_inst_dir, "transform.RDS"))
}, error = function(e) {
  assert(FALSE, "Failed to load transformation routine(transform.RDS) from %s: %s", model_inst_dir, e)
})

tryCatch({
  model_features <- unlist(jsonlite::read_json(path = file.path(model_inst_dir, "features.json")))
}, error = function(e) {
  assert(FALSE, "Failed to load model features(features.json) from %s: %s", model_inst_dir, e)
})

loginfo("... starting h2o ...")
h2o_conn <- H2OHelpers::init_h2o_conn()

tryCatch({
  loginfo("... loading the instance ....")
  model <- h2o::h2o.loadModel(path = file.path(model_inst_dir, model_inst_id))
}, error = function(e) {
  H2OHelpers::shutdown_h2o(h2o_conn)
  assert(FALSE, "Failed to load model instance file(%s) from %s: %s", model_inst_dir, model_inst_id, e)
})

loginfo("... done")

tryCatch({
  # retrieve data set definition
  dataset_def <- WhiskyCore::get_dataset_def(script_path)

  loginfo("Connecting to FS ...")
  conn <- FeatureStore::fs_connect(fs_id = dataset_def$fs_id)
  loginfo("... done")
}, error = function(e) {
  H2OHelpers::shutdown_h2o(h2o_conn)
  stop(e)
})

tryCatch({
  loginfo("Retrieving features ...")

  data_range_dt <- FeatureStore::fs_get_obj_data_range(conn, dataset_def$object)
  assert(!is.na(data_range_dt$till), "No features data to perform scoring!")

  score_month <- as.Date(data_range_dt$till)
  loginfo("... loading features for %s ...", score_month)

  feats_tbl <-
    FeatureStore::fs_get_feats_tbl(conn,
                                   object = dataset_def$object,
                                   features = c("customer_id", "month", model_features),
                                   data_range = list(from = score_month, till = score_month)) %>%
    collect()

  loginfo("... done; Transforming features ...")

  abt_hf <- preprocess_and_load_abt(abt = feats_tbl)

  loginfo("... done; Generating scorings ...")

  prediction <- h2o.predict(model, abt_hf)

  # Create table with scorings
  score_dest_month <- seq(from = score_month, by = "+1 month", length.out = 2)[[2]]
  scorings_tbl <- tibble(customer_id = feats_tbl$customer_id,
                         month = score_dest_month,
                         score = as.vector(prediction$p1))

  loginfo("... done; Saving forecast ...")

  score_dpath <- ScoringRepo::sr_save_scoring(
    model_inst_id, scoring_id, scoring_save_dir, scorings_tbl,
    tag = format(score_dest_month, "%Y-%m")
  )

  loginfo("All done. (saved into %s)", score_dpath)
},
error = function(e) {
  logerror("Failed: %s", conditionMessage(e))
  stop(e)
},
finally = {
  close(conn)
  H2OHelpers::shutdown_h2o(h2o_conn)
})

# retrieve internal PlumberAPI environment
api_env <- PlumberAPI:::get_internal_env()

#* Forces using new model instance id.
#* @param model_inst_id Instance id to use. NA to use latest instance
#* @post /model_instance
function(model_inst_id = NA_character_) {
  api_env$load_model_inst(model_inst_id)
}

#* Scoring information on the row.
#* @param row_id:int Id of row to scor (e.g. 2)
#* @get /score
function(row_id = 2L) {
  loginfo("calculating score for row id:%s", row_id)
  row_id <- as.integer(row_id)

  new_apartment <- api_env$get_score_frame(id = row_id)
  res <- api_env$score(new_apartment)

  return(list(score = res))
}

#* Calculates score for variables
#* @param district The district (e.g. Srodmiescie)
#* @param no.rooms:int Number of rooms (e.g. 2)
#* @param construction.year:int Then the building was constructed (e.g. 2005)
#* @param floor:int Which flow flat is on (e.g. 2)
#* @param surface:double Surface of the flat in square meters (e.g. 69)
#* @get /score_vars
function(district = "Srodmiescie", no.rooms = 2L, construction.year = 2000L, floor = 2L, surface = 69.0) {
  loginfo("calculating score for vars")

  new_apartment <- data.frame(
    construction_year = as.numeric(construction.year),
    surface = as.numeric(surface),
    floor = as.numeric(floor),
    no_rooms = as.numeric(no.rooms),
    district = district)
  res <- api_env$score(new_apartment)

  return(list(score = res))
}


#* Explanation of scoring on the row.
#* @param row_id:int Id of row to scor (e.g. 2)
#* @get /explain
function(row_id = 2L) {
  loginfo("calculating explanation for row id:%s", row_id)
  row_id <- as.integer(row_id)

  new_apartment <- api_env$get_score_frame(id = row_id)
  res <- api_env$explain(new_apartment)

  return(res)
}

#* Explanation of scoring for variables
#* @param district The district (e.g. Srodmiescie)
#* @param no.rooms:int Number of rooms (e.g. 2)
#* @param construction.year:int Then the building was constructed (e.g. 2005)
#* @param floor:int Which flow flat is on (e.g. 2)
#* @param surface:double Surface of the flat in square meters (e.g. 69)
#* @get /explain_vars
function(district = "Srodmiescie", no.rooms = 2L, construction.year = 2000L, floor = 2L, surface = 69.0) {
  loginfo("calculating explanation for vars")

  new_apartment <- data.frame(
    construction_year = as.numeric(construction.year),
    surface = as.numeric(surface),
    floor = as.numeric(floor),
    no_rooms = as.numeric(no.rooms),
    district = district)
  res <- api_env$explain(new_apartment)

  return(res)
}

import os
script_path = os.path.dirname(os.sys.argv[0]) if (os.sys.argv and os.sys.argv[0]) else "./scripts"
os.sys.path.insert(0, script_path)

from initenv import init_logger, load_config, args_parse, arg_spec, load_proj_info
log = init_logger()

# Load packages
import math, time, json, shutil
import pandas as pd
import numpy as np
import h2o

import flask, flask_restplus

import whisky_core, whisky_feature_store, whisky_h2o_helpers, whisky_flask

# Handle configuration and cmd line arguments
config = load_config()
args = args_parse(
    arg_spec(
        "--ModelInstLoadDir",
        dest = "model_inst_load_dir", default = config["ModelInstLoadDir"], required = False,
        help = "Folder to load model instance(s) from (default: <config:ModelInstLoadDir>)"),
    arg_spec(
        "--ListenPort",
        dest = "listen_port", default = config["ListenPort"], required = False,
        help = "Port REST Api is listening at"),
    arg_spec(
        "--ServiceInstId",
        dest = "serv_inst_id", default = None, required = False,
        help = "Service instance Id")
)
args.model_inst_load_dir = args.model_inst_load_dir.replace("<prj_root>", os.path.join(script_path, ".."))

# initialize flask APP
proj_info = load_proj_info()
api = flask_restplus.Api(
        version = proj_info.get("version", "1.0"),
        title = "{}: REST API".format(proj_info.get("name")))
whisky_flask.register_api_error_handlers(api)

@api.errorhandler
def default_error_handler(e):
    message = 'An unhandled exception occurred.'
    log.exception(message)

application = flask.Flask(proj_info.get("name", __name__))  #  Create a Flask WSGI application
application.wsgi_app = whisky_flask.ReverseProxied(application.wsgi_app, api)
application.config.update({
    "SWAGGER_UI_DOC_EXPANSION": "list",
    "RESTPLUS_VALIDATE": True,
    "RESTPLUS_MASK_SWAGGER": False,
    "ERROR_404_HELP": False
})
whisky_flask.set_swaggerui_statics_url_path()

app_ctx = {
    "h2o_model": None,
    "fs_conn": None,
    "data_tbl": None,
}

def create_app_ns(api):
    ns = api.namespace('', description='Application REST API')

    model_inst_post_parser = flask_restplus.reqparse.RequestParser()
    model_inst_post_parser.add_argument(
        "model_inst_id", type = str, location = "args", required = False,
        help = "Instance id to use. If not passed will use latest instance"
    )

    def _run_score(row_tbl):
        import time

        assert app_ctx["h2o_model"], "No model instance loaded"

        start_ts = time.time()
        log.debug("calculating scoring ...")

        newdata_h2o = h2o.H2OFrame(row_tbl)
        res = app_ctx["h2o_model"].predict(newdata_h2o)
        score = res[0, "predict"]
        log.debug("... done ({} secs)".format(time.time() - start_ts))
        return score

    @ns.route('/model_instance')
    # pylint: disable=unused-variable
    class ModelInstanceAction(flask_restplus.Resource):
        @api.expect(model_inst_post_parser)
        def post(self):
            """ Forces using new model instance id. """
            model_inst_id = model_inst_post_parser.parse_args().model_inst_id

            search_path = args.model_inst_load_dir
            if model_inst_id:
                search_path = os.path.join(search_path, model_inst_id)

            model_files = [
                os.path.join(dpath, fn)
                for dpath, _, fnames in os.walk(search_path)
                for fn in fnames
            ]
            assert len(model_files) > 0, "No trained model instances found in {}.".format(search_path)

            # order by creation time & select first
            latest_model_fpath = sorted(model_files, key = lambda fp: os.path.getctime(fp), reverse = True)[0]

            if not h2o.cluster():
                log.info(" ... initializing h2o")
                whisky_h2o_helpers.init_h2o_conn()

            log.info("Loading model instance from {} ...".format(latest_model_fpath))
            if app_ctx["h2o_model"]:
                log.info("... cleaning previously loaded model instance")
                h2o.remove_all()
                app_ctx["h2o_model"] = None

            log.info("... loading the model instance")
            app_ctx["h2o_model"] = h2o.load_model(path = latest_model_fpath)
            log.info(".. done.")

    score_get_parser = flask_restplus.reqparse.RequestParser()
    score_get_parser.add_argument(
        "row_id", type = int, location = "args", required = False, default = 2,
        help = "Id of row to score (e.g. 2)"
    )
    score_get_result = api.model("Scoring", {
        "score": flask_restplus.fields.Float(required = True, description = 'Calculated scoring'),
        "inst_id": flask_restplus.fields.String(required = False, description = 'Instance which generated the scoring'),
    })

    @ns.route('/score')
    # pylint: disable=unused-variable
    class ScoreAction(flask_restplus.Resource):
        @api.expect(score_get_parser)
        @api.marshal_with(score_get_result)
        def get(self):
            """ Scoring information on the row. """
            row_id = score_get_parser.parse_args().row_id

            data_tbl = app_ctx["data_tbl"]
            row_data = data_tbl.loc[data_tbl.id == row_id].drop(columns = ["id"]).head(1)

            score = _run_score(row_data)
            return { "score": score, "inst_id": args.serv_inst_id }

    score_vars_get_parser = flask_restplus.reqparse.RequestParser()
    score_vars_get_parser.add_argument(
        "district", type = str, location = "args", required = False, default = "Srodmiescie", help = "The district (e.g. Srodmiescie)")
    score_vars_get_parser.add_argument(
        "no_rooms", type = int, location = "args", required = False, default = 2, help = "Number of rooms (e.g. 2)")
    score_vars_get_parser.add_argument(
        "construction_year", type = int, location = "args", required = False, default = 2005, help = "Then the building was constructed (e.g. 2005)")
    score_vars_get_parser.add_argument(
        "floor", type = int, location = "args", required = False, default = 2, help = "Which flow flat is on (e.g. 2)")
    score_vars_get_parser.add_argument(
        "surface", type = float, location = "args", required = False, default = 69.0, help = "Surface of the flat in square meters (e.g. 69)")

    @ns.route("/score_vars")
    # pylint: disable=unused-variable
    class ScoreVarsAction(flask_restplus.Resource):
        @api.expect(score_vars_get_parser)
        @api.marshal_with(score_get_result)
        def get(self):
            """ Calculates score for variables. """
            req_args = score_vars_get_parser.parse_args()

            row_data = pd.DataFrame([
                [ getattr(req_args, feat) for feat in dataset_def.features ]
            ], columns = dataset_def.features)

            score = _run_score(row_data)
            return { "score": score, "inst_id": args.serv_inst_id }

    api.add_namespace(ns)

blueprint = flask.Blueprint('', proj_info.get("name", __name__))
api.init_app(blueprint)
create_app_ns(api)
application.register_blueprint(blueprint)

dataset_def = whisky_core.DataSetDef.load(script_path)
log.info("Connecting to FS[{}] ...".format(dataset_def.fs_id))

try:
    conn = whisky_feature_store.connect(fs_id = dataset_def.fs_id)
    app_ctx["fs_conn"] = conn

    log.info("... done; loading data ...")

    app_ctx["data_tbl"] = conn.get_feats_tbl(
        object_name = dataset_def.object,
        features = [ "id" ] + dataset_def.features
    )

    log.info("... done; starting REST API ...")

    application.run(port = int(args.listen_port), host = "0.0.0.0")
finally:
    log.info("Cleaning up ...")
    if app_ctx["fs_conn"]:
        app_ctx["fs_conn"].disconnect()
    if h2o.cluster():
        h2o.cluster().shutdown()
    log.info("... done")

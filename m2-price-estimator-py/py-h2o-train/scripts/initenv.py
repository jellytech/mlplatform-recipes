import logging.config

def load_config():
    import os, shutil, yaml

    script_path = os.path.dirname(__file__)
    conf_fpath = os.path.join(script_path, "..", "config.yaml")
    if not os.path.isfile(conf_fpath):
        shutil.copy(os.path.join(script_path, "..", "config_templ.yaml"), conf_fpath)

    with open(conf_fpath, "rt") as conf_f:
        return yaml.full_load(conf_f)

def load_proj_info():
    import os, json

    script_path = os.path.dirname(__file__)
    proj_fpath = os.path.join(script_path, "..", "project.json")
    try:
        with open(proj_fpath, "rt") as proj_f:
            return json.load(proj_f)
    except:
        return {}

def arg_spec(*args, **kwargs):
    return lambda parser: parser.add_argument(*args, **kwargs) 

def args_parse(*specs):
    import argparse
    argparser = argparse.ArgumentParser()

    for arg_spec in specs:
        arg_spec(argparser)
    if argparser.prog == "ipykernel_launcher.py":
        argparser.add_argument("-f", required = True, help = "ipykernel_launcher file to be launched")
    return argparser.parse_args()

def init_logger():
    log_config = load_config().get("logging", {})
    log_config.update({ "version": 1 })
    logging.config.dictConfig(log_config)
    return logging.getLogger()

import os
script_path = os.path.dirname(os.sys.argv[0]) if (os.sys.argv and os.sys.argv[0]) else "./scripts"
os.sys.path.insert(0, script_path)

from initenv import init_logger, load_config, args_parse, arg_spec
log = init_logger()

# Load packages
import math, time, json, shutil
import pandas as pd
import numpy as np
import h2o, h2o.automl

import whisky_core, whisky_feature_store, whisky_scoring_repo, whisky_h2o_helpers

# Handle configuration and cmd line arguments
config = load_config()
args = args_parse(
    arg_spec(
        "--ModelInstSaveDir",
        dest = "model_inst_save_dir", default = config["ModelInstSaveDir"], required = False,
        help = "Folder to save model instance into (default: <config:ModelInstSaveDir>)"),
    arg_spec(
        "--ModelInstId",
        dest = "model_inst_id", default = time.strftime("%Y%m%d_%H%M", time.gmtime()), required = False,
        help = "Identifier of model instance to generate (default: <current timestamp>)")
)
args.model_inst_save_dir = args.model_inst_save_dir.replace("<prj_root>", os.path.join(script_path, ".."))

dest_dir = os.path.join(args.model_inst_save_dir, args.model_inst_id)
if os.path.isdir(dest_dir):
    shutil.rmtree(dest_dir)
os.makedirs(dest_dir, exist_ok = True)

dataset_def = whisky_core.DataSetDef.load(script_path)
metrics_conf = whisky_core.MetricsConfig.load(script_path)

log.info("Connecting to FS[{}] ...".format(dataset_def.fs_id))

# conn = whisky_feature_store.connect(fs_id = dataset_def.fs_id)
with whisky_feature_store.connect(fs_id = dataset_def.fs_id) as conn:
    try:
        log.info("... done; Retrieving features ...")

        train_tbl = conn.get_feats_tbl(
            object_name = dataset_def.object,
            features = dataset_def.features + [ dataset_def.target ])

        log.info("... done; Starting H2O ...")

        whisky_h2o_helpers.init_h2o_conn()

        log.info("... done; Loading data into H2O ...")

        train_hf = h2o.H2OFrame(train_tbl)

        log.info("... done; Training model ...")

        aml = h2o.automl.H2OAutoML(max_models = 10, max_runtime_secs = 60)
        aml.train(
            x = dataset_def.features, y = dataset_def.target,
            training_frame = train_hf
        )

        log.info("... done; Validating model ...")

        metrics = whisky_scoring_repo.calc_eval_metrics(
            data_df = pd.DataFrame({
                "prediction": aml.leader.predict(train_hf).as_data_frame().predict,
                "target": train_tbl[dataset_def.target]
            }),
            config = metrics_conf.to_json()
        )

        log.info("... done; Saving the leader into %s ...", dest_dir)

        h2o.save_model(aml.leader, path = dest_dir)

        log.info("... done; Saving model metadata ...")

        whisky_scoring_repo.save_metrics(dest_dir, metrics)
        whisky_core.save_model_inst_meta(args.model_inst_id, args.model_inst_save_dir, script_path)

        log.info("All done.")
    except BaseException as ex:
        log.error("Failed: {}".format(ex))
        raise
    finally:
        if h2o.cluster():
            h2o.cluster().shutdown()

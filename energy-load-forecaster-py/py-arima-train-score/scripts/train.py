import os
script_path = os.path.dirname(os.sys.argv[0]) if (os.sys.argv and os.sys.argv[0]) else "./scripts"
os.sys.path.insert(0, script_path)

from initenv import init_logger, load_config, args_parse, arg_spec
log = init_logger()

# Load packages
import math, time, json, shutil
import pandas as pd
import numpy as np
import statsmodels.tsa.arima_model

import whisky_core, whisky_feature_store

# Handle configuration and cmd line arguments
config = load_config()
args = args_parse(
    arg_spec(
        "--ModelInstSaveDir",
        dest = "model_inst_save_dir", default = config["ModelInstSaveDir"], required = False,
        help = "Folder to save model instance into (default: <config:ModelInstSaveDir>)"),
    arg_spec(
        "--ModelInstId",
        dest = "model_inst_id", default = time.strftime("%Y%m%d_%H%M", time.gmtime()), required = False,
        help = "Identifier of model instance to generate (default: <current timestamp>)")
)
args.model_inst_save_dir = args.model_inst_save_dir.replace("<prj_root>", os.path.join(script_path, ".."))

dest_dir = os.path.join(args.model_inst_save_dir, args.model_inst_id)
if os.path.isdir(dest_dir):
    shutil.rmtree(dest_dir)
os.makedirs(dest_dir, exist_ok = True)

# TODO:
#   response from data_set_def
ylog_transform = False
target_feat = "log_avg_load" if ylog_transform else "avg_load"

dataset_def = whisky_core.DataSetDef.load(script_path)
log.info("Connecting to FS[{}] ...".format(dataset_def.fs_id))

# conn = whisky_feature_store.connect(fs_id = dataset_def.fs_id)
with whisky_feature_store.connect(fs_id = dataset_def.fs_id) as conn:
    log.info("... done; Retrieving features ...")

    try:
        obj_data_range = conn.get_obj_data_range(dataset_def.object)
        assert obj_data_range["from_ts"], "There is not enough data of {} for model training".format(dataset_def.object)

        hist_tbl = conn.get_feats_tbl(
            object_name = dataset_def.object,   # "load",
            features = dataset_def.features + [ target_feat ], # TODO: "yearly_mean_degc{year+1L}"
            data_range = obj_data_range)

        log.info("... done; Performing imputations ...")

        # default value for yearly_mean_degc
        total_yearly_mean_degc = conn.get_raw_tbl("temps_yearly")["yearly_mean_degc"].agg("mean")
        hist_tbl = hist_tbl.fillna({ "yearly_mean_degc": total_yearly_mean_degc })

        log.info("... done; Transforming features ...")

        hist_tbl = hist_tbl.drop(columns = [ "avg_temp_fc_degc" ]) # we do not need it for training. It is used for scoring only
        hist_tbl["season_adj_temp"] = hist_tbl.apply(
            lambda row: (2 * row["yearly_mean_degc"] - row["avg_temp_degc"]) if (row["winter"] == 1) else row["avg_temp_degc"],
            axis = 1
        )
        hist_tbl["log_avg_load"] = hist_tbl.apply(
            lambda row: math.log(row["avg_load"] + 1.0),
            axis = 1
        )

        log.info("... done; Splitting onto test/train ...")

        # TODO:
        # split <- FeatureStore::split_train_test(hist_tbl, test_period_hor = "-90 days")
        # split$train_tbl
        # split$train_end_date
        # split$test_tbl
        # split$test_end_date
        #

        hist_end_date = hist_tbl["date"].agg("max")
        test_period_start = hist_end_date - pd.Timedelta(days = 90)

        split = {
            "train_tbl": hist_tbl[hist_tbl.date < test_period_start].set_index("date"),
            "test_start_date": test_period_start,
            "test_end_date": hist_end_date,
            "test_tbl": hist_tbl[hist_tbl.date >= test_period_start].set_index("date"),
        }

        # Fit and test arima model
        # 1. Fit a model using auto.arima on training part of time series
        # 2. Make test forecast on test  part of time series
        # 3. Prepare results as a data.table

        log.info("Fitting auto.arima model...")
        arima = statsmodels.tsa.arima_model.ARIMA(
            split["train_tbl"][target_feat],
            order = (5, 1, 0), # (p,d,q) order ???
            exog = split["train_tbl"][["weekend", "season_adj_temp"]],
            freq = 'D') # daily data
        model = arima.fit(disp = 0)

        model_fpath = os.path.join(dest_dir, "model.pcl")
        log.info("... done. Saving model into {} ...".format(model_fpath))
        model.save(model_fpath)
        log.info("... done. Testing model ...")

        # fcast_test <- generateForecast(model, test_period_days, dt_xreg_vars = dt_hist[split == "test", ..xreg_var_names])
        # .. fcast <- forecast(model, h = fcast_horizon, xreg = dt_xreg_vars[1:fcast_horizon])
        fcast_test_tbl = model.predict(
            start = split["test_start_date"],
            end = split["test_end_date"],
            exog = split["test_tbl"][["weekend", "season_adj_temp"]],
            typ = "levels"
        )

        # dt_fcast_test <- postprocessForecasts(dates = dt_hist[split == "test", date], fcast = fcast_test, ylog_transform = ylog_transform)
        if ylog_transform:
            fcast_test_tbl = fcast_test_tbl.apply(lambda x: math.exp(float(x) - 1.0))

        calc_rmse = lambda act, pred: np.mean((pred - act)**2)**.5
        infos = {
            "rmse_test": np.mean((fcast_test_tbl - split["test_tbl"][dataset_def.target]) ** 2) ** .5,
            "ylog_applied": ylog_transform,
        }
        infos_fpath = os.path.join(dest_dir, "infos.json")
        log.info("... done. Saving model infos into {} ...".format(infos_fpath))

        whisky_core.save_model_inst_meta(args.model_inst_id, args.model_inst_save_dir, script_path)
        with open(infos_fpath, "wt") as inf_f:
            json.dump(infos, inf_f, indent = 2)

        log.info("All done.")
    except BaseException as ex:
        log.error("Failed: {}".format(ex))
        raise

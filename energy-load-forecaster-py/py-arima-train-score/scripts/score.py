import os
script_path = os.path.dirname(os.sys.argv[0]) if (os.sys.argv and os.sys.argv[0]) else "./scripts"
os.sys.path.insert(0, script_path)

from initenv import init_logger, load_config, args_parse, arg_spec
log = init_logger()

# Load packages
import time, json, math
import pandas as pd
import numpy as np
import statsmodels.tsa.arima_model

import whisky_core, whisky_feature_store, whisky_scoring_repo

# Handle configuration and cmd line arguments
config = load_config()
args = args_parse(
    arg_spec(
        "--ModelInstLoadDir",
        dest = "model_inst_load_dir", default = config["ModelInstLoadDir"], required = False,
        help = "Folder to load model instance from (default: <config:ModelInstLoadDir>)"),
    arg_spec(
        "--ModelInstId",
        dest = "model_inst_id", default = None, required = False,
        help = "Identifier of model instance to generate (default: <current timestamp>)"),
    arg_spec(
        "--ScoringId",
        dest = "scoring_id", default = time.strftime("%Y%m%d_%H%M_scoring", time.gmtime()), required= False,
        help = "Identifier of scoring to generate (default: <current timestamp>_scoring)"),
    arg_spec(
        "--ScoringSaveDir",
        dest = "scoring_save_dir", default = config["ScoringSaveDir"], required= False,
        help = "Folder to save scoring into (default: <config:ScoringSaveDir>)")
)
args.model_inst_load_dir = args.model_inst_load_dir.replace("<prj_root>", os.path.join(script_path, ".."))
assert \
    os.path.isdir(args.model_inst_load_dir), \
    "Model instance load folder({}) does not exist".format(args.model_inst_load_dir)

if not args.model_inst_id:
    inst_files = [
        os.path.join(dpath, fn)
        for dpath, _, fnames in os.walk(args.model_inst_load_dir)
        for fn in fnames if fn == "model.pcl"
    ]
    assert len(inst_files) > 0, "No trained model instances found in {}.".format(args.model_inst_load_dir)

    inst_files = sorted(inst_files, key = lambda fp: os.path.getctime(fp), reverse = True)

    model_inst_fpath = inst_files[0]
    model_inst_id = os.path.basename(os.path.dirname(model_inst_fpath))
else:
    model_inst_id = args.model_inst_id
    model_inst_fpath = os.path.join(args.model_inst_load_dir, model_inst_id, "model.pcl")
    assert \
        os.path.isfile(model_inst_fpath), \
        "Model({}) not found in {}. (expected model.pcl does not exist)".format(
            model_inst_id, args.model_inst_load_dir)

args.scoring_save_dir = args.scoring_save_dir.replace("<prj_root>", os.path.join(script_path, ".."))

log.info("Loading model ...")

model = statsmodels.tsa.arima_model.ARIMAResults.load(model_inst_fpath)

# retrieve 'ylog_applied' from model info
info_fpath = os.path.join(os.path.dirname(model_inst_fpath), "infos.json")
ylog_applied = False
try:
    with open(info_fpath, "rt") as inf_f:
        ylog_applied = json.load(inf_f).get("ylog_applied")
except BaseException:
    log.warning("Failed to read model information from {}; using defaults".format(info_fpath))

# retrieve data set definition
dataset_def = whisky_core.DataSetDef.load(script_path)
log.info("... done; Connecting to FS[{}] ...".format(dataset_def.fs_id))

# conn = whisky_feature_store.connect(fs_id = dataset_def.fs_id)
with whisky_feature_store.connect(fs_id = dataset_def.fs_id) as conn:
    log.info("... done; Retrieving features ...")

    try:
        fcast_start_date = conn.get_obj_data_range(dataset_def.object).get("till_ts")
        assert \
            fcast_start_date, \
            "There is not enough data of {} for model training".format(dataset_def.object)

        fcast_start_date = pd.Timestamp(fcast_start_date) + pd.Timedelta(days = 1)

        fcast_end_date = conn.get_obj_data_range("temp_fcast").get("till_ts")
        if fcast_end_date:
            fcast_end_date = pd.Timestamp(fcast_end_date)
        assert\
            fcast_end_date and fcast_start_date <= fcast_end_date, \
            "not enough temperature forecast data to perform load forecast"

        temps_fcast_tbl = conn.get_feats_tbl(
            object_name = dataset_def.object, # "load",
            features = dataset_def.features, # TODO: "yearly_mean_degc{year+1L}"
            data_range = { "from_ts": fcast_start_date, "till_ts": fcast_end_date }
        )

        log.info("... done; Performing imputations ...")

        # default value for yearly_mean_degc
        total_yearly_mean_degc = conn.get_raw_tbl("temps_yearly")["yearly_mean_degc"].agg("mean")

        temps_fcast_tbl["avg_temp_degc"] = temps_fcast_tbl.apply(
            lambda row: row["avg_temp_fc_degc"] if pd.isnull(row["avg_temp_degc"]) else row["avg_temp_degc"],
            axis = 1
        )
        temps_fcast_tbl = temps_fcast_tbl.fillna({ "yearly_mean_degc": total_yearly_mean_degc })
        temps_fcast_tbl = temps_fcast_tbl.drop(columns = [ "avg_temp_fc_degc" ])

        log.info("... done; Transforming features ...")

        temps_fcast_tbl["season_adj_temp"] = temps_fcast_tbl.apply(
            lambda row: (2 * row["yearly_mean_degc"] - row["avg_temp_degc"]) if (row["winter"] == 1) else row["avg_temp_degc"],
            axis = 1
        )

        log.info("... done; Generating forecast ...")

        loads_fcast_arr, _, _ = model.forecast(
            steps = temps_fcast_tbl.shape[0],
            exog = temps_fcast_tbl[["weekend", "season_adj_temp"]])
        loads_fcast_sr = pd.Series(loads_fcast_arr)

        if ylog_applied:
            loads_fcast_sr = loads_fcast_sr.apply(lambda x: math.exp(float(x) - 1.0))

        loads_fcast_tbl = pd.concat([ temps_fcast_tbl["date"], loads_fcast_sr ], axis = 1, sort = False)
        loads_fcast_tbl.columns = [ "date", "avg_load" ]

        log.info("... done; Saving forecast ...")

        score_dpath = whisky_scoring_repo.save_scoring(
            model_inst_id, args.scoring_id, args.scoring_save_dir,
            loads_fcast_tbl
        )

        log.info("All done. (saved to {})".format(score_dpath))
    except BaseException as ex:
        log.error("Failed: {}".format(ex))
        raise

import os
script_path = os.path.dirname(os.sys.argv[0]) if (os.sys.argv and os.sys.argv[0]) else "./scripts"
os.sys.path.insert(0, script_path)

from initenv import init_logger, load_config, args_parse, arg_spec
log = init_logger()

# Load packages
import math, time, json, shutil
import pandas as pd
import numpy as np

import whisky_core, whisky_feature_store

# Handle configuration and cmd line arguments
# ...

def perform_transforms(conn):
    hour_cols = [ "h{}".format(h) for h in range(1, 25) ] # hour columns

    log.info("transforming & updating loads_daily ...")

    load_hist_tbl = conn.get_raw_tbl("raw_historical_load_hourly")
    load_hist_tbl["date"] = pd.to_datetime(
        load_hist_tbl["year"].astype(str) \
        + "-" + load_hist_tbl["month"].astype(str) \
        + "-" + load_hist_tbl["day"].astype(str)
    )

    # limiting to required range
    max_loads_date = conn.get_obj_data_range("load").get("till_ts")
    if max_loads_date is not None:
        load_hist_tbl = load_hist_tbl[ load_hist_tbl.date > pd.to_datetime(max_loads_date.date()) ]

    if not load_hist_tbl.empty:
        load_daily_tbl = load_hist_tbl[["date"]].copy()
        load_daily_tbl["avg_load"] = load_hist_tbl.apply(lambda row: np.mean([ row[hc] for hc in hour_cols ]), axis = 1)

        conn.write_obj_feats_tbl("load", load_daily_tbl)

    log.info("... done; transforming & updating temps_daily ...")

    temp_hist_tbl = conn.get_raw_tbl("raw_historical_temp_hourly")
    temp_hist_tbl["date"] = pd.to_datetime(
        temp_hist_tbl["year"].astype(str) \
        + "-" + temp_hist_tbl["month"].astype(str) \
        + "-" + temp_hist_tbl["day"].astype(str)
    )

    # limiting to required range
    max_temps_date = conn.get_obj_data_range("temp").get("till_ts")
    if max_temps_date is not None:
        temp_hist_tbl = temp_hist_tbl[ temp_hist_tbl.date > pd.to_datetime(max_temps_date.date()) ]

    if not temp_hist_tbl.empty:
        temp_daily_tbl = temp_hist_tbl[["date"]].copy()
        temp_daily_tbl["avg_temp_degf"] = temp_hist_tbl.apply(lambda row: np.mean([ row[hc] for hc in hour_cols ]), axis = 1)
        temp_daily_tbl["avg_temp_degc"] = (temp_daily_tbl["avg_temp_degf"] - 32.0) / 1.8

        conn.write_obj_feats_tbl("temp", temp_daily_tbl)


    log.info("... done; transforming & updating temps_daily_forecasts ...")

    temp_raw_fcast_tbl = conn.get_raw_tbl("raw_forecasted_temp_daily")
    temp_raw_fcast_tbl["date"] = pd.to_datetime(temp_raw_fcast_tbl["date"])

    # limiting to required range
    max_temps_fcast_date = conn.get_obj_data_range("temp_fcast").get("till_ts")
    if max_temps_fcast_date is not None:
        temp_raw_fcast_tbl = temp_raw_fcast_tbl[ temp_raw_fcast_tbl.date > pd.to_datetime(max_temps_fcast_date.date()) ]

    if not temp_raw_fcast_tbl.empty:
        temp_fcast_tbl = temp_raw_fcast_tbl[[ "date", "avg_temp_degf" ]].copy()
        temp_fcast_tbl.columns = [ "date", "avg_temp_fc_degf" ]
        temp_fcast_tbl["avg_temp_fc_degc"] = (temp_fcast_tbl["avg_temp_fc_degf"] - 32.0) / 1.8

        conn.write_obj_feats_tbl("temp_fcast", temp_fcast_tbl)


    log.info("... done; updating temps_yearly ...")

    max_full_year = conn.get_raw_tbl("temps_yearly")["year"].max()

    temp_daily_tbl = conn.get_raw_tbl("temps_daily")
    if not pd.isna(max_full_year):
        temp_daily_tbl = temp_daily_tbl[temp_daily_tbl.date.dt.year > max_full_year]

    temp_yearly_tbl = temp_daily_tbl.groupby(temp_daily_tbl.date.dt.year).agg(
        n = ("date", "count"),
        yearly_mean_degf = ("avg_temp_degf", "mean"),
        yearly_mean_degc = ("avg_temp_degc", "mean")
    ).reset_index()
    temp_yearly_tbl = temp_yearly_tbl[temp_yearly_tbl.n >= 365]\
        .drop(columns = ["n"])\
        .rename(columns = { "date": "year" })

    conn.write_obj_feats_tbl("temps_yearly", temp_yearly_tbl)

    log.info("... done; updating date_features ...")

    max_date = conn.get_raw_tbl("date_features")["date"].max()
    if pd.isna(max_date):
        first_dates = list(filter(None, [
            conn.get_obj_data_range("load").get("from_ts"),
            conn.get_obj_data_range("temp").get("from_ts"),
            conn.get_obj_data_range("temp_fcast").get("from_ts")
        ]))
        if not first_dates:
            log.info("... done")
            return

        first_date = pd.to_datetime(np.min(first_dates))
    else:
        first_date = max_date + pd.Timedelta(days = 1)

    last_dates = list(filter(None, [
        conn.get_obj_data_range("load").get("till_ts"), max_loads_date,
        conn.get_obj_data_range("temp").get("till_ts"), max_temps_date,
        conn.get_obj_data_range("temp_fcast").get("till_ts"), max_temps_fcast_date
    ]))
    last_date = np.max(last_dates)

    if first_date.date() > last_date.date():
        log.info("... done")
        return

    date_feats_tbl = pd.DataFrame(pd.date_range(start = first_date, end = last_date, freq = "1D"), columns = [ "date" ])
    date_feats_tbl["year"] = date_feats_tbl["date"].dt.year
    date_feats_tbl["weekend"] = date_feats_tbl["date"].apply(lambda d: 1 if (d.weekday() in [5, 6]) else 0)
    date_feats_tbl["winter"] = date_feats_tbl["date"].apply(lambda d: 1 if (d.month in [1, 2, 3, 11, 12]) else 0)

    conn.write_obj_feats_tbl("date", date_feats_tbl)

    log.info("... done")


dataset_def = whisky_core.DataSetDef.load(script_path)
log.info("Connecting to FS[{}] ...".format(dataset_def.fs_id))

# conn = whisky_feature_store.connect(fs_id = dataset_def.fs_id)
with whisky_feature_store.connect(fs_id = dataset_def.fs_id) as conn:
    log.info("... done")
    try:
        perform_transforms(conn)
        log.info("All done.")
    except BaseException as ex:
        log.error("Failed: {}".format(ex))
        raise
